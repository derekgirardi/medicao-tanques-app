import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CadastrarComponent } from './cadastrar/cadastrar.component';
import { GraficoComponent } from './grafico/grafico.component';
import { AplicativoComponent } from './aplicativo/aplicativo.component';

const routes: Routes = [
  {path: '', component: AplicativoComponent},
  {path: 'cadastrar', component: CadastrarComponent},
  {path: 'grafico', component: GraficoComponent}
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

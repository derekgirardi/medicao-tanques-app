import { Component, OnInit } from '@angular/core';
import { Chart } from 'chart.js';

@Component({
  selector: 'grafico',
  templateUrl: './grafico.component.html',
  styleUrls: [ './grafico.component.css' ]
})

export class GraficoComponent implements OnInit {

  lineChart1: any = [];
  lineChart2: any = [];

  constructor() {}

  ngOnInit() {

    this.criarLineChart1();
    this.criarLineChart2();
  }

  criarLineChart1() {
    this.lineChart1 = new Chart('line-chart1', {
      type: 'line',
      data: {
        labels: [1500,1600,1700,1750,1800,1850,1900,1950,1999,2050],
        datasets: [{ 
            data: [86,114,106,106,107,111,133,221,783,2478],
            label: "dataset 1",
            borderColor: "#3e95cd",
            fill: false
          }, { 
            data: [282,350,411,502,635,809,947,1402,3700,5267],
            label: "dataset 2",
            borderColor: "#8e5ea2",
            fill: false
          }
        ]
      },
      options: {
        title: {
          display: true,
          text: 'Graph title 1'
        }
      }
    });
  }

  criarLineChart2() {
    this.lineChart2 = new Chart('line-chart2', {
      type: 'line',
      data: {
        labels: [1500,1600,1700,1750,1800,1850,1900,1950,1999,2050],
        datasets: [{ 
            data: [86,114,106,106,107,111,133,221,783,2478],
            label: "dataset 1",
            borderColor: "#3e95cd",
            fill: false
          }, { 
            data: [5267,3700,1402,947,809,635,502,411,350,282],
            label: "dataset 2",
            borderColor: "#8e5ea2",
            fill: false
          }
        ]
      },
      options: {
        title: {
          display: true,
          text: 'Graph title 2'
        }
      }
    });
  }
}

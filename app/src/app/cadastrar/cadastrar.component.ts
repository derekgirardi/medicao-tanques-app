import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'cadastrar',
  templateUrl: './cadastrar.component.html',
  styleUrls: ['./cadastrar.component.css']
})
export class CadastrarComponent {

  volume: string;
  codigo: string;
  produto: string;

    constructor(private router: Router) {}

    atribuirVolume(volume) {
      this.volume = volume;
    }

    atribuirCodigo(codigo) {
      this.codigo = codigo;
    }

    atribuirProduto(produto) {
      this.produto = produto;
    }

    salvar() {
      console.log("Salvar");
      console.log(this.volume);
      console.log(this.codigo);
      console.log(this.produto);
    }

    cancelar() {
      console.log("Cancelar");
    }
}

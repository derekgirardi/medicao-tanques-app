import { Component, Input, ElementRef, ViewChild } from '@angular/core';
import { Square } from '../aplicativo/Square'

@Component({
  selector: 'card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.css']
})

export class CardComponent {

  @ViewChild("medidor") medidor: ElementRef; 

  @Input() square: Square;
  @Input() volume: string;
  @Input() codigo: string;
  @Input() produto: string;
  @Input() classes: string = "col-sm-4 col-md-2";

  constructor() {}

  ngAfterViewInit() {
    let ctx: CanvasRenderingContext2D = this.medidor.nativeElement.getContext("2d");
    this.square = new Square(ctx);
    this.square.moveRight();
  }
}

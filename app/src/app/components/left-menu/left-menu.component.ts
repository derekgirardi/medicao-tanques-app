import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { onSideNavChange, animateText } from '../../animations/animations'
import { SidenavService } from '../../services/sidenav.service'

interface Page {
  link: string;
  name: string;
  icon: string;
}

@Component({
  selector: 'app-left-menu',
  templateUrl: './left-menu.component.html',
  styleUrls: ['./left-menu.component.css'],
  animations: [onSideNavChange, animateText]
})
export class LeftMenuComponent implements OnInit {

  public sideNavState: boolean = false;
  public linkText: boolean = false;

  public pages: Page[] = [
    {name: 'Home', link:'', icon: "home"},
    {name: 'Cadastrar', link:'/cadastrar', icon: "post_add"},
    {name: 'Gráfico', link:'/grafico', icon: "assessment"},
  ]

  constructor(private _sidenavService: SidenavService, private router: Router) { }

  ngOnInit() {
  }

  onSinenavToggle() {
    this.sideNavState = !this.sideNavState
    
    setTimeout(() => {
      this.linkText = this.sideNavState;
    }, 200)
    this._sidenavService.sideNavState$.next(this.sideNavState)
  }

  ButtonClick(page: Page) {
    console.log(page.link);
    this.router.navigate([page.link]);
    return;
  }
}
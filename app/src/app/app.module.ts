import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CardComponent } from './card/card.component';
import { AplicativoComponent } from './aplicativo/aplicativo.component';
import { CadastrarComponent } from './cadastrar/cadastrar.component';
import { GraficoComponent } from './grafico/grafico.component';
import { CommonModule } from '@angular/common';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { SidenavService } from './services/sidenav.service';
import { AplicativoService } from './aplicativo/aplicativo.service';

import { HeaderComponent } from './components/header/header.component';
import { LeftMenuComponent } from './components/left-menu/left-menu.component';
import { MatIconModule, MatListModule, MatSidenavModule } from '@angular/material';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    LeftMenuComponent,
    CardComponent,
    AplicativoComponent,
    CadastrarComponent,
    GraficoComponent
  ],
  imports: [
    BrowserAnimationsModule,
    BrowserModule,
    FormsModule,
    MatIconModule,
    MatListModule,
    MatSidenavModule,
    AppRoutingModule,
    CommonModule
  ],
  providers: [
    SidenavService,
    AplicativoService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

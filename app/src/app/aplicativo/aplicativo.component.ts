import { Component } from '@angular/core';
import { AplicativoService } from "./aplicativo.service";

@Component({
  selector: 'aplicativo',
  templateUrl: './aplicativo.component.html',
  styleUrls: [ './aplicativo.component.css' ]
})

export class AplicativoComponent  {

  constructor(private aplicativoService: AplicativoService) {}

  get tanques() {
    return this.aplicativoService.tanques;
  }
}
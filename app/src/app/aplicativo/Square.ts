export class Square {
    private backgroundColor = "lightgreen";
    private color = '#efefef';
    private z = 80;

    constructor(private ctx: CanvasRenderingContext2D) {}

    moveRight() {
        this.drawBackground();
        this.drawContent();
        this.drawBorders();
    }

    private drawBackground() {
        this.ctx.fillStyle = this.backgroundColor;
        this.ctx.fillRect(5, 5, this.z, this.z + 30);
    }

    private drawContent() {
        this.ctx.fillStyle = this.color;
        this.ctx.fillRect(5, 5, this.z, this.z - 20);   
    }

    private drawBorders() {
        this.ctx.strokeRect(5, 5, this.z, this.z + 30);
    }
}
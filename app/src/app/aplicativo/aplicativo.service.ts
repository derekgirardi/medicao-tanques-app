import { Injectable } from '@angular/core';
import { CardComponent } from '../card/card.component';

@Injectable()
export class AplicativoService {

    tanques: CardComponent[] = [];

    constructor () {
        this.tanques.push(this.buildCard(
            "5.235/10.000 Litros",
            "Tanque Reserva C12",
            "Óleo"
        ));

        this.tanques.push(this.buildCard(
            "5.235/10.000 Litros",
            "Tanque Reserva C14",
            "Óleo"
        ));

        this.tanques.push(this.buildCard(
            "5.235/10.000 Litros",
            "Tanque Reserva A12",
            "Óleo"
        ));

        this.tanques.push(this.buildCard(
            "5.235/10.000 Litros",
            "Tanque Reserva T32",
            "Óleo"
        ));

        this.tanques.push(this.buildCard(
            "5.235/10.000 Litros",
            "Tanque Reserva C02",
            "Óleo"
        ));

        this.tanques.push(this.buildCard(
            "5.235/10.000 Litros",
            "Tanque Reserva C93",
            "Óleo"
        ));

        this.tanques.push(this.buildCard(
            "5.235/10.000 Litros",
            "Tanque Reserva F44",
            "Óleo"
        ));

        this.tanques.push(this.buildCard(
            "5.235/10.000 Litros",
            "Tanque Reserva B09",
            "Óleo"
        ));

        this.tanques.push(this.buildCard(
            "5.235/10.000 Litros",
            "Tanque Reserva A13",
            "Óleo"
        ));
    }

    private buildCard(volume:string, codigo: string, produto: string) {
        let tanque: CardComponent = new CardComponent();
        tanque.volume   = volume;
        tanque.codigo   = codigo;
        tanque.produto  = produto;

        return tanque;
    }
}
